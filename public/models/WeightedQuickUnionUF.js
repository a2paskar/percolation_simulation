class WeightedQuickUnionUF
{
    // Create an empty union find data structure with N isolated sets.
     constructor(N)
    {
        this.count = N; // number of components
        this.id = new Array(N); // id[i] = parent of i
        this.sz = new Array(N); // sz[i] = number of objects in subtree rooted at i
        for (var i = 0; i < N; i++)
        {
            this.id[i] = i;
            this.sz[i] = 1;
        }
    }

    // Return the number of disjoint sets.
    count()
    {
        return this.count;
    }

    // Return component identifier for component containing p
    find(p)
    {
        while (p != this.id[p])
            p = this.id[p];
        return p;
    }

    // Are objects p and q in the same set?
    connected(p, q)
    {
        return this.find(p) === this.find(q);
    }

    // Replace sets containing p and q with their union.
    union( p, q)
    {
        var i = this.find(p);
        var j = this.find(q);
        if (i === j)
            return;

        // make smaller root point to larger one
        if (this.sz[i] < this.sz[j])
        {
            this.id[i] = j;
            this.sz[j] += this.sz[i];
        }
        else
        {
            this.id[j] = i;
            this.sz[i] += this.sz[j];
        }
        this.count--;
    }

}
