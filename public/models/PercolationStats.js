import("./Percolation.js");

const table = document.querySelector(".MyTable");

const simulateButton = document.querySelector(".simulate");
var everyObject = [];
var fullsites=[];

function searchForArray(haystack, needle){
    var i, j, current;
    for(i = 0; i < haystack.length; ++i){
      if(needle.length === haystack[i].length){
        current = haystack[i];
        for(j = 0; j < needle.length && needle[j] === current[j]; ++j);
        if(j === needle.length)
          return i;
      }
    }
    return -1;
  }


function draw(x,y, i) { 
    setTimeout(function() { 
        // Add tasks to do
        var rows = table.getElementsByTagName("tr");
        console.log(searchForArray(fullsites,[x,y])==0);
        if (searchForArray(fullsites,[x,y])!=-1){
            rows[x-1].cells[y-1].className= "fullsite";
        } else{
            rows[x-1].cells[y-1].className= "opensite";
        }

    }, 100*i); 
 }
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

class PercolationStats {
    // perform independent trials on an n-by-n grid
    constructor( n) {
        this.sim = new Percolation(n);
        while (!this.sim.percolates()) {

            var row = getRandomInt(1, n + 1);
            var col = getRandomInt(1, n + 1);
            if (!this.sim.isOpen(row, col)){
                this.sim.open(row, col);
                everyObject.push([row,col]);

            }

        }
        for (var i=0;i<everyObject.length;i++){
            if (this.sim.isFull(everyObject[i][0], everyObject[i][1])){
                fullsites.push(everyObject[i]);

            }
        }

    }
}

(function setup(){
    simulateButton.addEventListener("click", function(){
        
        Monte_Carlo = new PercolationStats(8);
        
        for(var i = 0; i< everyObject.length; i++){

            draw(everyObject[i][0],everyObject[i][1], i); 
        }

        console.log("all_openL:");
        console.log(everyObject);
        console.log("full:");
        console.log(fullsites);

    });

}())
