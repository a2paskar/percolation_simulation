import("./WeightedQuickUnionUF.js");


class Percolation{
    constructor(n){
        if (n<=0){
            console.log("Error");
        } else{
            this.id= Array(n).fill().map(() => Array(n).fill(0));
            this.UF = new WeightedQuickUnionUF(n*n+2); //implement this data structure
            this.closed_sites=n*n;
        }
    }
    isOpen(row,col){
        var a = row > this.id.length;
        var b = row < 0;
        var c = col > this.id.length;
        var d = col < 0;
        if(a && b && c && d){
            console.log("ERROR");
        }
        return this.id[row - 1][col - 1] === 1;
    }

    open(row, col){
        var a = row >this.id.length;
        var b = row<0;
        var c= col>this.id.length;
        var d= col<0;

        if(a&&b&&c&&d){
            console.log("ERROR");
        }
        if (this.id[row - 1][col - 1] != 1) {
            this.id[row - 1][col - 1] = 1;
            this.closed_sites -= 1;

            if (row === this.id.length)
                this.UF.union(this.id.length * (row - 1) + col, this.id.length * this.id.length + 1);
            if (row === 1)
                this.UF.union(this.id.length * (row - 1) + col, 0);


            if (row - 1 > 0)
                if (this.isOpen(row - 1, col)) {
                    this.UF.union(this.id.length * (row - 1) + col, this.id.length * (row - 2) + col);
                }
            if (row + 1 <= this.id.length)
                if (this.isOpen(row + 1, col)) {
                    this.UF.union(this.id.length * (row - 1) + col, this.id.length * (row) + col);
                }

            if (col + 1 <= this.id.length)
                if (this.isOpen(row, col + 1)) {
                    this.UF.union(this.id.length * (row - 1) + col, this.id.length * (row - 1) + col + 1);
                }
            if (col - 1 > 0)
                if (this.isOpen(row, col - 1)) {
                    this.UF.union(this.id.length * (row - 1) + col, this.id.length * (row - 1) + col - 1);
                }
        }

    }

    isFull(row,col){
        var a = row > this.id.length;
        var b = row < 0;
        var c = col > this.id.length;
        var d = col < 0;

        if (a && b && c && d){
            console.log("ERROR");
        }
        if (this.isOpen(row, col)) {
            if (this.UF.find(this.id.length * (row - 1) + col) === this.UF.find(0)) { //canonical elements of given site is equal to canonical of VS1
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }
    numberOfOpenSites(){
        return this.id.length*this.id.length-this.closed_sites;
    }

    // does the system percolate?
    percolates() {
        return (this.UF.find(this.id.length * this.id.length + 1) == this.UF.find(0));
    }

}


// var chichi= new Percolation(5);
// chichi.open(1,2);
// console.log(chichi.isOpen(1,2));
// console.log(chichi.percolates());


// console.log(chichi.isOpen(2,2));
// chichi.open(2,2);
// console.log(chichi.percolates());

// chichi.open(3,2);
// console.log(chichi.isOpen(3,2));
// console.log(chichi.percolates());

// chichi.open(4,2);
// console.log(chichi.isOpen(4,2));
// console.log(chichi.percolates());

// console.log(chichi.isOpen(5,2));
// chichi.open(5,2);
// console.log(chichi.isOpen(5,2));
// console.log(chichi.percolates());