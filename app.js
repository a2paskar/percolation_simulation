//SETUP
var express= require("express");
var app=express();

app.use(express.static("public"));// this line is so that our css files which will be in our public dir will be accessed through our link tags in our ejs files!!
app.set("view engine", "ejs");//instead of explicitly typing home.ejs, we can simply write home.  NOT NECESSARY

/*
//CONNECT TO DB
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/Percolation_DB', {
  useNewUrlParser: true,
  useUnifiedTopology: true
}) //we can set db_name to name of db, if not created it will be created.  Need to be running daemon for this to work
.then(() => console.log('Connected to DB!'))
.catch(error => console.log(error.message));
//this shit is all copy paste stuff to configure mongodb and connect to it 

//DB data config
var percolationSchema = new mongoose.Schema({
	title:String,
	image:String,
	body:String,
	created:{type:Date, default: Date.now} //add a default created attributed if none entered!
});

var Percolation = mongoose.model("Percolation", percolationSchema);//what we use to interact with db ex) finding all records, etc
*/

//ROUTES

//WELCOME SCREEN ~ scroll down = next page ~ OOS
app.get("/", function(req,res){
	//get data here to send in?
	res.redirect("/Percolation");
	
});

app.get("/Percolation", function(req,res){
	//get data here to send in?
	
	res.render("home");
	
});

//WHAT IS PERCOLATION? ~ scroll for next page ~ ATP
app.get("/AboutPercolation", function(req,res){
	//get data here to send in?
	
	//show simulation of percolation
	res.render("about");
	
});

//Tutorial page for how to use the page ~ OOS
app.get("/Tutorial", function(req,res){
	//get data here to send in?
	res.render("home");
});


//Interactive page ~ ATP
app.get("/Play", function(req,res){
	//get data here to send in?
	
	//Grid (preset or user defined)
	
	//Create your own vs. preset (ex. black box to picture of rock lee) then start button to start flow of water
	//Once percolation is reached, it will say percolation is reached or not possible
	
	//Stats button ~ monte carlo simulation .593 shit
	res.render("play");
});


//LISTEN
app.listen(3000, function(){
	console.log("SERVER IS RUNNING");
});